cmake_minimum_required(VERSION 3.16)

include(../../Common/CMakeLists.txt)

set( PROJECT_NAME PluginLinker )

project( ${PROJECT_NAME} )

find_package(Qt6 COMPONENTS Core)
find_package(Qt6 COMPONENTS Gui)
find_package(Qt6 COMPONENTS Widgets)

qt_add_plugin( ${PROJECT_NAME} SHARED )
set( SOURCES
    ../../Interfaces/ipluginlinker.h
    linkeritembase.cpp
    pluginlinker.cpp
    pluginlinkeritem.cpp
)
set( MOC_HEADERS
    pluginlinker.h
    pluginlinkeritem.h
    pluginlinkeritemdescriptor.h
    linkeritembase.h
)
include(../../Common/Plugin/CMakeLists.txt)
qt_wrap_cpp( SOURCES ${MOC_HEADERS} )
target_sources( ${PROJECT_NAME} PRIVATE ${SOURCES} )
target_link_libraries( ${PROJECT_NAME} PUBLIC
    Qt::Core
    Qt::Gui
    Qt::Widgets
)
