#include "pluginlinkeritem.h"

#include "pluginlinkeritemdescriptor.h"

const QMap<QString, QStringList> META_INFO_STRUCTURE =
{
	{"interfaces", {}},
	{"references", {}},
	{"info", {"name", "about"}},
};

PluginLinkerItem::PluginLinkerItem(IPluginHandlerPtr pluginHandler) :
	LinkerItemBase(pluginHandler)
{
	connect(this, &LinkerItemBase::onReferencesChanged, this, &PluginLinkerItem::connectionsChanged);
}

PluginLinkerItem::~PluginLinkerItem()
{

}

IReferenceDescriptorPtr PluginLinkerItem::descr()
{
	if(m_descriptor.isNull())
		return m_pluginDescriptor;
	else
		return m_descriptor;
}

const QMap<Interface, int> &PluginLinkerItem::references()
{
	return m_pluginDescriptor->references();
}

bool PluginLinkerItem::init()
{
	auto plugin = m_pluginHandler.toStrongRef();
	m_pluginDescriptor.reset( PluginItemDescriptor::make(plugin->getUID(), plugin->getMeta()) );
	m_descriptor = m_pluginDescriptor;
	return !m_pluginDescriptor.isNull();
}

bool PluginLinkerItem::isPlugin(IPluginHandlerPtr pluginHandler)
{
	auto metaInfoObject = pluginHandler.toStrongRef()->getMeta();
	QJsonObject metaInfo = metaInfoObject.toStrongRef()->value("MetaData").toObject();
	metaInfo = metaInfo.value("iplugin").toObject();
	for(auto iter = META_INFO_STRUCTURE.begin(); iter != META_INFO_STRUCTURE.end(); ++iter)
	{
		if(!metaInfo.contains(iter.key()))
		{
			qDebug() << QString("PluginBase::parseMetaInfo: meta has no field '%1' but has fields:").arg(iter.key());
			for(auto allFieldsIter = metaInfo.begin(); allFieldsIter != metaInfo.end(); ++allFieldsIter)
			{
				qDebug() << QString("%1: %2").arg(allFieldsIter.key()).arg(allFieldsIter.value().toString());
			}
			return false;
		}
	}
	return true;
}

QString PluginLinkerItem::initItem(QObject* object)
{
	auto instance = qobject_cast<IPlugin *>(object);

	if(!instance)
	{
		return QString("can't cast plugin to IPlugin interface.");
	}

	auto&& handler = m_pluginHandler.toStrongRef();
	if(!instance->isInited() && !instance->pluginInit(handler->getUID(), handler->getMeta()))
	{
		instance->pluginFini();
		return "Can't load";
	}

	m_descriptor = instance->getDescriptor();
	if(m_descriptor.isNull())
	{
		//        return QString("internal plugin error: %1").arg(instance->getLastError());
		return QString("internal plugin error");
	}
	else
	{
		m_pluginInstance.reference()->setInstance(m_descriptor);
		return QString();
	}
}

void PluginLinkerItem::setupReferences()
{
	//	qDebug() << "Setting references for:" << descr().toStrongRef()->name();
	const auto &handler = m_pluginInstance->getInstancesHandler();
	for(auto iter = m_references->begin(); iter != m_references->end(); ++iter)
	{
		QList<IReferenceDescriptorPtr> refs;
		for (auto refIter = iter.value().begin(); refIter != iter.value().end(); ++refIter)
		{
			auto descr = refIter->toStrongRef()->descr();
			Q_ASSERT(descr.toStrongRef()->object());
			refs.append(descr);
		}
		//		qDebug() << "+" << iter.key() << ":" << refs;
		handler.toStrongRef()->setReferences(iter.key(), refs);
	}
}

QString PluginLinkerItem::finiItem()
{
	m_pluginInstance->pluginFini();
	m_pluginInstance.reset();
	return QString();
}

void PluginLinkerItem::finalizeLinkage()
{
	m_pluginInstance->getInstancesHandler().toStrongRef()->transitToReadyState();
}

void PluginLinkerItem::connectionsChanged(quint32 selfUID, quint32 itemUID, bool isAdded)
{
	if(m_pluginInstance.reference()->isSet())
	{
		setupReferences();
	}
}
