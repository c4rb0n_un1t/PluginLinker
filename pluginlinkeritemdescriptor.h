#pragma once

#include "../../Common/Plugin/ireferenceshandler.h"

class PluginItemDescriptor : public IReferenceDescriptor
{
public:
	static PluginItemDescriptor* make(quint32 uid, QWeakPointer<QJsonObject> meta)
	{
		auto metaData = meta.toStrongRef()->value("MetaData").toObject();
		auto section = metaData.value("iplugin").toObject();

		auto&& infoSection = section.value("info").toObject();
		auto&& name = infoSection.value("name").toString();
		auto&& about = infoSection.value("about").toString();
		if(name.isEmpty() || about.isEmpty())
		{
			qDebug() << QString("PluginBase::parseMetaInfo: parse error: fields name or about is empty.");
			return nullptr;
		}

		auto iterfacesJSONArr = section.value("interfaces").toArray();
		QVector<Interface> interfaces;
		interfaces.reserve(iterfacesJSONArr.size());
		for(auto iterfaceJSON : iterfacesJSONArr)
		{
			auto interface = iterfaceJSON.toString();
			if(interface.isEmpty())
			{
				qDebug() << QString("PluginBase::parseMetaInfo: interface entry is empty.");
				return nullptr;
			}
			interfaces.append(Interface(interface));
		}

		QMap<Interface, int> references;
		auto referencesJSONMap = section.value("references").toObject();
		for(auto iter = referencesJSONMap.begin(); iter != referencesJSONMap.end(); ++iter)
		{
			auto&& referenceName = iter.key();
			auto referencesCount = iter.value().toInt(-1);
			if(referenceName.isEmpty() || referencesCount == -1)
			{
				qDebug() << QString("PluginBase::parseMetaInfo: reference entry is empty or reference count is invalid.");
				return nullptr;
			}
			references[Interface(referenceName)] = referencesCount;
		}

		auto ptr = new PluginItemDescriptor();
		ptr->m_uid = uid;
		ptr->m_name = name;
		ptr->m_about = about;
		ptr->m_instance = nullptr;
		ptr->m_interfaces = interfaces;
		ptr->m_references = references;

		//        qDebug() << "PluginDescriptor::make: created reference descriptor:" <<
		//            "uid:" << uid << endl <<
		//            "name:" << name << endl <<
		//            "about:" << about << endl <<
		//            "instance:" << instance << endl <<
		//            "interfaces:" << interfaces << endl <<
		//            "references:" << referencesNames << endl;

		return ptr;
	}
private:
	PluginItemDescriptor() = default;
public:
	virtual ~PluginItemDescriptor() = default;

	// IReferenceDescriptor interface
public:
	inline virtual quint32 uid() override
	{
		return m_uid;
	}
	inline virtual const QVector<Interface> &interfaces() override
	{
		return m_interfaces;
	}
	inline virtual const QString &name() override
	{
		return m_name;
	}
	inline virtual const QString &about() override
	{
		return m_about;
	}
	inline virtual QObject *object() override
	{
		return m_instance;
	}

public:
	inline void setObject(QObject* instance)
	{
		m_instance = instance;
	}
	inline QMap<Interface, int>& references()
	{
		return m_references;
	}

private:
	quint32 m_uid;
	QString m_name;
	QString m_about;
	QObject* m_instance;
	QVector<Interface> m_interfaces;
	QMap<Interface, int> m_references;
};


