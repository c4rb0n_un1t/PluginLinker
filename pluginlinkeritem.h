#pragma once


#include <QObject>
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

#include <QList>
#include <QMap>
#include <QHash>

#include "linkeritembase.h"

#include "../../Common/Plugin/iplugin.h"
#include "../../Common/Plugin/referenceinstance.h"

class PluginItemDescriptor;

class PluginLinkerItem : public LinkerItemBase
{
	Q_OBJECT
public:
	PluginLinkerItem(IPluginHandlerPtr pluginHandler);
	virtual ~PluginLinkerItem() override;

	// ILinkerItem interface
public:
	IReferenceDescriptorPtr descr() override;
	virtual const QMap<Interface, int>& references() override;

	// LinkerItemBase interface
public:
	bool init() override;
	void setupReferences() override;

protected:
	virtual QString initItem(QObject *object) override;
	virtual QString finiItem() override;
	void finalizeLinkage() override;

private slots:
	void connectionsChanged(quint32 selfUID, quint32 itemUID, bool isAdded);

public:
	static bool isPlugin(IPluginHandlerPtr pluginHandler);

private:
	ReferenceInstancePtr<IPlugin> m_pluginInstance;
	QSharedPointer<PluginItemDescriptor> m_pluginDescriptor;
};


